#From https://askubuntu.com/questions/664684/how-to-convert-all-the-mov-videos-to-mp4-in-folder#665305
#Transcodes mov files by apple to mp4 files in directory out/
mkdir out
for fname in *.MOV; do ffmpeg -i "$fname" -acodec copy -vcodec libx265 -crf 28  out/"${fname%.MOV}.mp4"; done
