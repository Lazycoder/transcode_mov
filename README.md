From https://askubuntu.com/questions/664684/how-to-convert-all-the-mov-videos-to-mp4-in-folder#665305
Transcodes mov files by apple to mp4 files in directory out/
Thanks to llogan  at askubuntu.com https://askubuntu.com/users/59378/llogan

As this code is in the public domain, please credit myself and llogan if you reuse it.
